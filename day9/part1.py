
def check_if_min(el, i, j, grid):
    for y, x in [[-1, 0], [1, 0], [0, 1], [0, -1]]:
        if (new_i := i + y) < 0 or new_i >= len(grid) or (new_j := j +x) < 0 or new_j >= len(grid[i]):
            continue
        if el >= grid[new_i][new_j]:
            return False
    return True

if __name__ == "__main__":
    grid = []
    with open("input.txt", "r") as f:
        for line in f:
            grid.append([int(x) for x in line.rstrip()])
    total = 0
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            el = grid[i][j]
            if check_if_min(el, i, j, grid):
                total += el + 1

    print(total)