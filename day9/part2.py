
def check_if_min(el, i, j, grid):
    for y, x in [[-1, 0], [1, 0], [0, 1], [0, -1]]:
        if (new_i := i + y) < 0 or new_i >= len(grid) or (new_j := j +x) < 0 or new_j >= len(grid[i]):
            continue
        if el >= grid[new_i][new_j]:
            return False
    return True

def get_adjacent(i, j, grid):
    adj = []
    for y, x in [[-1, 0], [1, 0], [0, 1], [0, -1]]:
        if (new_i := i + y) < 0 or new_i >= len(grid) or (new_j := j +x) < 0 or new_j >= len(grid[i]):
            continue
        if grid[new_i][new_j] == 9:
            continue
        adj.append((new_i, new_j))
    return adj

def get_basin(grid, check=[], checked=[]):
    for i, j in check:
        if (i, j) not in checked:
            checked.append((i, j))
            get_basin(grid, get_adjacent(i, j, grid), checked)
    return checked

if __name__ == "__main__":
    grid = []
    with open("input.txt", "r") as f:
        for line in f:
            grid.append([int(x) for x in line.rstrip()])
    basins = []
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            el = grid[i][j]
            if check_if_min(el, i, j, grid):
                basins.append(get_basin(grid, get_adjacent(i, j, grid), [(i, j)]))
    basins = sorted(basins, key=len, reverse=True)
    total = 1
    for l in basins[:3]:
        total *= len(l)
    print(total)