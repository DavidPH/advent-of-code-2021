from collections import defaultdict

def fold(grid, w, h, instruction):
    axis, index = instruction
    if axis == "x":
        x_range = index
    else:
        x_range = w + 1
    if axis == "y":
        y_range = index
    else:
        y_range = h + 1
    
    for y in range(y_range):
        for x in range(x_range):
            if axis == "y":
                new_y = 2*index - y
                grid[(x, y)] += grid[(x, new_y)]
                grid[(x, new_y)] = 0
            elif axis == "x":
                new_x = 2*index - x
                grid[(x, y)] += grid[(new_x, y)]
                grid[(new_x, y)] = 0
    if axis == "x":
        return x_range, w
    if axis == "y":
        return w, y_range

if __name__ == "__main__":
    height = 0
    width = 0
    grid = defaultdict(int)
    instructions = []
    with open("input.txt", "r") as f:
        for line in f:
            line = line.rstrip()
            if line:
                cord = line.split(",")
                if len(cord) == 2:
                    x, y = map(int, cord)
                    if x > width:
                        width = x 
                    if y > height:
                        height = y 
                    grid[(x, y)] += 1
                else:
                    axis, index = line.split("=")
                    instructions.append((axis[-1], int(index)))
    
    for instruction in instructions:
        width, height = fold(grid, width, height, instruction)
    for y in range(height):
        for x in range(width):
            if grid[(x, y)] >= 1:
                print(" █ ", end="")
            else:
                print("   ", end="")
        print()

