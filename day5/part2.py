from collections import defaultdict

def betterRange(x, y):
    return range(x, y + 1) if y > x else range(x, y - 1, -1)

if __name__ == "__main__":
    lines = []
    with open('./input.txt', 'r') as f:
        for line in f:
            ((x1, y1), (x2, y2)) = map(lambda e: list(
                map(int, e.split(","))), line.rstrip().split(" -> "))
            lines.append(((x1, y1), (x2, y2)))
    
    coords = defaultdict(int)
    for ((x1, y1), (x2, y2)) in lines: 
        if x1 == x2:
            for y in betterRange(y1, y2):
                coords[(x1, y)] += 1
        elif y1 == y2:
            for x in betterRange(x1, x2):
                coords[(x, y1)] += 1
        else:
            for x, y in zip(betterRange(x1, x2), betterRange(y1, y2)):
                coords[(x, y)] += 1

    print(len([val for val in coords.values() if val > 1]))