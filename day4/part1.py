
class bingoNumber:
    def __init__(self, no):
        self.number = no
        self.isMarked = False

    def __eq__(self, __o: object):
        return self.number == __o

    def __int__(self):
        return int(self.number)

    def __bool__(self):
        return self.isMarked

    def __repr__(self):
        return f"{self.number}{ '*' if self.isMarked else ''}".rjust(3)


class bingoBoard:
    def __init__(self):
        self.rows = []

    def append(self, bingoNumbers):
        self.rows.append(bingoNumbers)

    def markNumber(self, number):
        for row in self.rows:
            for num in row:
                if num == number:
                    num.isMarked = True

    def getScore(self):
        score = 0
        for row in self.rows:
            for num in row:
                if not num:
                    score += int(num)
        return score

    def isWinner(self):
        # Check rows
        count = 0
        for row in self.rows:
            if (count == 5):
                return True
            for num in row:
                if num:
                    count += 1
                else:
                    count = 0
                    break

        count = 0
        # Check columns
        for j in range(len(self.rows)):
            for row in self.rows:
                if row[j]:
                    count += 1
                    if (count >= 5):
                        return True
                else:
                    count = 0
                    break

        return False

    def __getitem__(self, key):
        return self.rows[key]

    def __str__(self) -> str:
        return "\n".join([str(r) for r in self.rows])


if __name__ == "__main__":
    with open('./input.txt', 'r') as f:
        lines = [line for l in f.readlines() if (line := l.rstrip()) != ""]

    drawn_lots = lines[0].split(",")
    boards = []

    for i, row in enumerate(lines[1::]):
        if i % 5 == 0:
            boards.append(bingoBoard())

        boards[-1].append([bingoNumber(e) for r in row.split(" ")
                          if (e := r.rstrip()) != ""])

    for num in drawn_lots:
        for board in boards:
            board.markNumber(num)
            if winner := board.isWinner():
                print(f"BINGO {num}")
                print(board.getScore() * int(num))
                print(board)
                break
        if winner:
            break
