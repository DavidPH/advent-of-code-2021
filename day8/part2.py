
def decode(inp, out):
    # Get 1, 4, 7, 8 
    digit_map = {}
    for digit in inp:
            digit = sorted(digit)
            match len(digit): 
                case 2:
                    digit_map[1] = digit
                
                case 4:
                    digit_map[4] = digit
                
                case 3:
                    digit_map[7] = digit
                
                case 7:
                    digit_map[8] = digit

    for digit in inp:
            digit = sorted(digit)
            if digit in digit_map.values():
                continue
            if all(e in digit for e in digit_map[1]): # it is either 0, 3, or 9
                if all(e in digit for e in digit_map[4]): # it is 9
                    digit_map[9] = digit
                elif all(e in digit for e in digit_map[7]):
                    if len(digit) == 6:
                        digit_map[0] = digit
                    else:
                        digit_map[3] = digit
            else: # it is either 2, 5, or 6
                if len(digit) == 6:
                    digit_map[6] = digit
                else:
                    # if it has 5 in common with 6 then it is 5, else it's a 2
                    if len([e for e in digit_map[6] if e in digit]) == 5:
                        digit_map[5] = digit
                    else:
                        digit_map[2] = digit
    output_map = {"".join(v):k for k,v in digit_map.items()}
    dec = 0
    for i, digit in enumerate(out):
        digit = "".join(sorted(digit))
        dec += output_map[digit] * 10**(3-i)
    return dec


if __name__ == "__main__":
    total = 0
    with open("input.txt", "r") as f:
        for line in f:
            inp = line.rstrip().split(" | ")[0].split(" ")
            out = line.rstrip().split(" | ")[1].split(" ")
            inp = sorted(inp, key=len, reverse=True)
            total += decode(inp, out)

    print(total)
    