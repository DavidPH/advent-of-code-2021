
most_common = []
starts_with = {'0': [], '1': []}


def get_most_common(bits_list):
    out = []
    for bits in bits_list:
        for i, b in enumerate(list(bits.rstrip())):
            if i >= len(out):
                out.append({"0": 0, "1": 0})
            out[i][b] += 1
    return out


def getratings(curr_selection, index, comp, default_select):
    if len(curr_selection) == 1:
        return curr_selection[0]
    select = ""
    som = []
    most_common = get_most_common(curr_selection)
    if most_common[index]["1"] == most_common[index]["0"]:
        select = default_select
    else:
        select = comp(most_common[index].keys(),
                      key=lambda k: most_common[index][k])
    j = [x for x in curr_selection if x[index] == select]
    return getratings(j, index + 1, comp, default_select)


if __name__ == "__main__":
    with open('./input.txt', 'r') as f:
        lines = [l.rstrip() for l in f.readlines()]

    oxygen = int(getratings(lines, 0, max, "1"), 2)
    co2 = int(getratings(lines, 0, min, "0"), 2)

    print(oxygen*co2)
