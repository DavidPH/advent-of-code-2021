
def get_most_common(bits_list):
    out = []
    for bits in bits_list:
        for i, b in enumerate(list(bits.rstrip())):
            if i >= len(out):
                out.append({"0": 0, "1": 0})
            out[i][b] += 1
    return out


if __name__ == "__main__":
    with open('./input.txt', 'r') as f:
        lines = [l.rstrip() for l in f.readlines()]
    most_common = get_most_common(lines)
    gamma = int("".join([max(bits.keys(), key=lambda k: bits[k])
                for bits in most_common]), 2)
    epsilon = int("".join([min(bits.keys(), key=lambda k: bits[k])
                           for bits in most_common]), 2)

    print(gamma*epsilon)
