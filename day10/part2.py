
if __name__ == "__main__":
    openers = {"(": ")", "[": "]", "{": "}", "<": ">"}
    closers = {")": "(", "]": "[", "}": "{", ">": "<"}
    scoring = {")": 1, "]": 2, "}": 3, ">": 4}
    scores = []
    with open("input.txt", "r") as f:
        for line in f:
            line = line.rstrip()
            last_open = []
            for i, char in enumerate(line):
                if char in closers.values():
                    last_open.append(char)
                elif char in closers.keys():
                    if closers[char] == last_open[-1]:
                        last_open.pop()
                    elif closers[char] != last_open[-1]:
                        break
            else: # Didn't break, aka not corrupted.
                total = 0
                for char in reversed(last_open):
                    total *= 5
                    total += scoring[openers[char]]
                scores.append(total)
    print(sorted(scores)[(len(scores) // 2)])

    
    
    