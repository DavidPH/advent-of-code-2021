
if __name__ == "__main__":
    closers = {")": "(", "]": "[", "}": "{", ">": "<"}
    scores = {")": 3, "]": 57, "}": 1197, ">": 25137}
    total = 0
    with open("input.txt", "r") as f:
        for line in f:
            line = line.rstrip()
            last_open = []
            for i, char in enumerate(line):
                if char in closers.values():
                    last_open.append(char)
                elif char in closers.keys():
                    if closers[char] == last_open[-1]:
                        last_open.pop()
                    elif closers[char] != last_open[-1]:
                        total += scores[char]
                        break
    print(total)
    