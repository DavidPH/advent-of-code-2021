
curr_pos = [0, 0]
if __name__ == "__main__":
    with open('./input.txt', 'r') as f:
        for line in f:
            line = line.rstrip()
            match line.rstrip().split(" "):
                case ["forward", x]:
                    curr_pos[0] += int(x)
                case ["up", y]:
                    curr_pos[1] -= int(y)
                case ["down", y]:
                    curr_pos[1] += int(y)

print(curr_pos[0] * curr_pos[1])
