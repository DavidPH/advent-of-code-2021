
curr_pos = [0, 0]
aim = 0
if __name__ == "__main__":
    with open('./input.txt', 'r') as f:
        for line in f:
            line = line.rstrip()
            match line.rstrip().split(" "):
                case ["forward", x]:
                    curr_pos[0] += int(x)
                    curr_pos[1] += int(x) * aim
                case ["up", y]:
                    aim -= int(y)
                case ["down", y]:
                    aim += int(y)

print(curr_pos[0] * curr_pos[1])
