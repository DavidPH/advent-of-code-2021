
def get_adjacent(i, j, grid):
    adj = []
    for y, x in [[-1, 0], [1, 0], [0, 1], [0, -1], [-1, 1], [1, 1], [-1, -1], [1, -1]]:
        if (new_i := i + y) < 0 or new_i >= len(grid) or (new_j := j +x) < 0 or new_j >= len(grid[i]):
            continue
        adj.append((new_i, new_j))
    return adj

def flash(i, j, grid):
    adj = get_adjacent(i, j, grid)
    grid[i][j] += 1
    if grid[i][j] == 10:
        for i, j in adj:
            flash(i, j, grid)

def do_steps(grid, count=1):
    flash_count = 0
    for _ in range(count):
        for i in range(len(grid)):
                for j in range(len(grid[i])):
                    flash(i, j, grid)
        for i in range(len(grid)):
                for j in range(len(grid[i])):
                    if grid[i][j] >= 10:
                        flash_count += 1
                        grid[i][j] = 0

    return flash_count   

if __name__ == "__main__":
    grid = []
    with open("input.txt", "r") as f:
        for line in f:
            grid.append([int(x) for x in line.rstrip()])
    
    print(do_steps(grid, 100))
    