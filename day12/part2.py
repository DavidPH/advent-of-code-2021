from collections import defaultdict
from collections import deque
from collections import Counter

def BFS(graph, start, end):
    queue = deque()
    queue.append([start])
    count = 0
    while queue:
        path = queue.popleft()
        last = path[-1]

        if last == end:
            count += 1
            continue
        for node in graph[last]:
            if node.isupper() or node not in path:
                queue.append(path + [node])
            elif node in path and node != "start" and node != "end":
                if all(c <= 1 for node, c in Counter(path).items() if node.islower()):
                    queue.append(path + [node])
    return count

if __name__ == "__main__":
    caves = defaultdict(set)
    with open("input.txt", "r") as f:
        for line in f:
            line = line.rstrip()
            c1, c2 = line.split("-")
            caves[c1].add(c2)
            caves[c2].add(c1)
    print(BFS(caves, "start", "end"))