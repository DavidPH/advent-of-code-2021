
if __name__ == "__main__":
    with open("input.txt", "r") as f:
        positions = [int(x) for x in f.readline().rstrip().split(",")]
    
    max_pos = max(positions)
    max_cost = None
    for i in range(max_pos + 1):
        costs = []
        for num in positions:
            costs.append(abs(i - num))
        s = sum(costs)
        if max_cost is None or s < max_cost:
            max_cost = s
    print(max_cost)