

if __name__ == '__main__':
    window_list = [[], [], []]  # [ [a1, a2, a3],  [b1, b2, b3], [c1, c2, c3] ]
    prev_sum = None
    depth_increases = 0
    with open('./input.txt', 'r') as f:
        for i, line in enumerate(f):
            line = int(line)
            for j, window in enumerate(window_list):
                if len(window) == 3:
                    s = sum(window)
                    if prev_sum is not None and s > prev_sum:
                        depth_increases += 1
                    prev_sum = s
                    window.clear()

                if j == 0:
                    window.append(line)
                if j - 1 >= 0:
                    if len(window_list[j - 1]) > 1 or i >= 3:
                        window.append(line)
        # Get leftovers
        for window in window_list:
            if len(window) == 3:
                if sum(window) > prev_sum:
                    depth_increases += 1

print(depth_increases)
