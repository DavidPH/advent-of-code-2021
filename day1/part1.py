
if __name__ == '__main__':
    depth_increases = 0
    prev = None
    with open('./input.txt', 'r') as f:
        for line in f:
            if (prev is not None and int(line) > prev):
                depth_increases += 1
            prev = int(line)

print(depth_increases)
