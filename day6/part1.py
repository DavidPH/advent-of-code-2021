
if __name__ == "__main__":
    fishes = []
    with open("input.txt", "r") as f:
        fishes = [int(line.rstrip()) for line in f.readline().split(",")]
    
    days = 80
    for i in range(days):
        baby_fishes = []
        for i in range(len(fishes)):
            fishes[i] -= 1
            if fishes[i] < 0:
                fishes[i] = 6
                baby_fishes.append(8)
        fishes += baby_fishes
    print(len(fishes))
        
        