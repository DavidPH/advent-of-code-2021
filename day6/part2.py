if __name__ == "__main__":
    fishes = [0]*9
    with open("input.txt", "r") as f:
        for fish in f.readline().split(","):
            fishes[int(fish)] += 1
    
    def cycle(fishes, days):
        for i in range(days):
            new_fishes = [0] * 9
            for i in range(8, -1, -1):
                if i == 0:
                    new_fishes[6] += fishes[i]
                    new_fishes[8] = fishes[i]
                else:
                    new_fishes[i - 1] = fishes[i]
            fishes = new_fishes
        print(sum(fishes))
    
    cycle(fishes, 256)